package com.nmk.ibank.service.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Entity
@Table(name = "ibank_account")
@EntityListeners(AuditingEntityListener.class)
public class IbankAccount {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idx;

	@Size(min = 2, message = "name should not be UNIQUE!")
	@Column(unique=true )
	private String name;

	private String description;

	@Column(name = "open_date", nullable = false, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date openDate;

	@Column(name = "closed_date ", nullable = false, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@CreatedDate
	private Date closedDate;

	@Column(name = "location_idx ")
	@NotNull
	private int locationIdx;

	@Column(name = "user_uid")
	@NotNull
	private int userUId;

	@Column(name = "created_by ")
	@Size(min = 2, message = "createdBy can't empty! should have atleast 2 characters")
	private String createdBy;

	@Column(name = "create_date", nullable = false, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@CreatedDate
	private Date createDate;

	@Column(name = "modified_by ")
	private String modifiedBy;

	@Column(name = "modified_date")
	@Temporal(TemporalType.TIMESTAMP)
	@LastModifiedDate
	private Date modifiedDate;

	@Column(name = "ibank_account_type_idx ")
	@NotNull
	private int ibankAccountTypeIdx;

	@EnableTransactionManagement
	public class ConfigurationBean {
	}

	public int getIdx() {
		return idx;
	}

	public void setIdx(int idx) {
		this.idx = idx;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getOpenDate() {
		return openDate;
	}

	public void setOpenDate(Date openDate) {
		this.openDate = openDate;
	}

	public Date getClosedDate() {
		return closedDate;
	}

	public void setClosedDate(Date closedDate) {
		this.closedDate = closedDate;
	}

	public int getLocationIdx() {
		return locationIdx;
	}

	public void setLocationIdx(int locationIdx) {
		this.locationIdx = locationIdx;
	}

	public int getUserUId() {
		return userUId;
	}

	public void setUserUId(int userUId) {
		this.userUId = userUId;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public int getIbankAccountTypeIdx() {
		return ibankAccountTypeIdx;
	}

	public void setIbankAccountTypeIdx(int ibankAccountTypeIdx) {
		this.ibankAccountTypeIdx = ibankAccountTypeIdx;
	}

}
