package com.nmk.ibank.web.service.intf;

import java.util.List;

import com.nmk.ibank.web.vo.AuditLogVo;






public interface IbankAuditlogService {

	
	void createAuditlog(AuditLogVo AuditLogVo);

	void updateAuditlog(AuditLogVo AuditLogVo);

	void deleteAuditlog(int id);

	AuditLogVo getAuditlogByIdx(int id);

	List<AuditLogVo> getAllAuditLog();
	
}
