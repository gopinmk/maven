package com.nmk.ibank.web.util;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;

import com.nmk.ibank.model.AuditLog;
import com.nmk.ibank.web.vo.AuditLogVo;



public class AuditLogWebUtil {
	
	
	
	public static AuditLog convertAuditLogVoObjectToAuditLog(AuditLogVo AuditLogVo) {
		AuditLog auditLog = new AuditLog();
		try {
			BeanUtils.copyProperties(auditLog, AuditLogVo);
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new RuntimeException(e);
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new RuntimeException(e);
		}
		return auditLog;
	}

	public static AuditLogVo convertAuditLogObjectToAuditLogVo(AuditLog  auditLog) {
		AuditLogVo AuditLogVo = new AuditLogVo();
		try {
			BeanUtils.copyProperties(AuditLogVo, auditLog);
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new RuntimeException(e);
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new RuntimeException(e);
		}
		return AuditLogVo;
	}
	public static List<AuditLogVo> convertAuditLogListToAuditLogVoList(List<AuditLog> auditLogs) {
		List<AuditLogVo> auditLogList = new ArrayList<AuditLogVo>();
		try {
			for (AuditLog auditLog : auditLogs) {
				AuditLogVo AuditLogVo = new AuditLogVo();
				BeanUtils.copyProperties(AuditLogVo, auditLog);
				auditLogList.add(AuditLogVo);
			}
		}  catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new RuntimeException(e);
		} catch (InvocationTargetException e){
			// TODO Auto-generated catch block
			e.printStackTrace();
			new RuntimeException(e);
		}
		return auditLogList;
	}

	


}
