package com.nmk.ibank.web.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.nmk.ibank.api.client.AuditlogServiceClient;
import com.nmk.ibank.model.AuditLog;

import com.nmk.ibank.web.service.intf.IbankAuditlogService;
import com.nmk.ibank.web.util.AuditLogWebUtil;
import com.nmk.ibank.web.vo.AuditLogVo;


@Service
public class IbankAuditlogimpl implements IbankAuditlogService {

	private static final Logger logger = Logger.getLogger(IbankAuditlogimpl.class);
	
	public void createAuditlog(AuditLogVo AuditLogVo) {
		// TODO Auto-generated method stub
		if (logger.isDebugEnabled()) logger.debug("Start: IbankAuditlogimpl.createAuditlog()");
		AuditLog auditLog = AuditLogWebUtil.convertAuditLogVoObjectToAuditLog(AuditLogVo);
		AuditlogServiceClient.createAuditlog(auditLog);
		if (logger.isDebugEnabled()) logger.debug("End: IbankAuditlogimpl.createAuditlog()");

	}

	public void updateAuditlog(AuditLogVo AuditLogVo) {
		if (logger.isDebugEnabled()) logger.debug("Start: IbankAuditlogimpl.updateBrand()");
		AuditLog auditLog = AuditLogWebUtil.convertAuditLogVoObjectToAuditLog(AuditLogVo);
		AuditlogServiceClient.updateAuditlog(auditLog);
		if (logger.isDebugEnabled()) logger.debug("End: IbankAuditlogimpl.updateBrand()");
		
	}

	public void deleteAuditlog(int id) {
		// TODO Auto-generated method stub
		if (logger.isDebugEnabled()) logger.debug("Start: IbankAuditlogimpl.deleteAuditlog()");	
		AuditlogServiceClient.deleteAuditlog(id);
		if (logger.isDebugEnabled()) logger.debug("End: IbankAuditlogimpl.deleteAuditlog()");
		
		
	}

	public AuditLogVo getAuditlogByIdx(int id) {
		// TODO Auto-generated method stub
		if (logger.isDebugEnabled()) logger.debug("Start: IbankAuditlogimpl.getAuditlogByIdx()");	
		 AuditLog auditLog = AuditlogServiceClient.getAuditlogByIdx(id);
		 if (logger.isDebugEnabled()) logger.debug("End: IbankAuditlogimpl.getAuditlogByIdx()");
		 return AuditLogWebUtil.convertAuditLogObjectToAuditLogVo(auditLog);
	}

	public List<AuditLogVo> getAllAuditLog() {
		// TODO Auto-generated method stub
		return null;
	}

	
}
