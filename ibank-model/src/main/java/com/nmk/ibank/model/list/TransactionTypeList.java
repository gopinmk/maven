package com.nmk.ibank.model.list;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.nmk.ibank.model.Transaction;
import com.nmk.ibank.model.TransactionType;

@XmlRootElement(name = "transaction")
public class TransactionTypeList extends DataList<TransactionType> { 

		@XmlElement(name = "transaction")
		@JsonProperty(value="transaction")
		public List<TransactionType> getData() {
			return data;	
		}

}
