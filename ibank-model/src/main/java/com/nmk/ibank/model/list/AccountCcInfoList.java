package com.nmk.ibank.model.list;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.nmk.ibank.model.AccountCcInfo;

@XmlRootElement(name = "AccountCcInfo")
public class AccountCcInfoList extends DataList<AccountCcInfo> {

	@XmlElement(name = "AccountCcInfo")
	@JsonProperty(value = "AccountCcInfo")
	public List<AccountCcInfo> getData() {
		return data;
	}

}
