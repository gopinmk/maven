package com.nmk.ibank.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessorOrder;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorOrder
public class Transaction implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private int idx;

	private String transactionId;
	private int transactionType;
	private Date transactionDate;
	private BigDecimal transAmount;
	private String description;
	private int accountIdx;
	private int parentIdx;
	private String createdBy;
	private Date createDate;
	private String modifiedBy;
	private Date modifiedDate;

	public Transaction() {
		super();
	}

	
	public Transaction(String transactionId, int transactionType, Date transactionDate, BigDecimal transAmount,
			String description, int accountIdx, int parentIdx, String createdBy, Date createDate, String modifiedBy,
			Date modifiedDate) {
		super();
		this.transactionId = transactionId;
		this.transactionType = transactionType;
		this.transactionDate = transactionDate;
		this.transAmount = transAmount;
		this.description = description;
		this.accountIdx = accountIdx;
		this.parentIdx = parentIdx;
		this.createdBy = createdBy;
		this.createDate = createDate;
		this.modifiedBy = modifiedBy;
		this.modifiedDate = modifiedDate;
	}


	public int getIdx() {
		return idx;
	}

	public void setIdx(int idx) {
		this.idx = idx;
	}


	public String getTransactionId() {
		return transactionId;
	}


	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}


	public int getTransactionType() {
		return transactionType;
	}


	public void setTransactionType(int transactionType) {
		this.transactionType = transactionType;
	}


	public Date getTransactionDate() {
		return transactionDate;
	}


	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}


	public BigDecimal getTransAmount() {
		return transAmount;
	}


	public void setTransAmount(BigDecimal transAmount) {
		this.transAmount = transAmount;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public int getAccountIdx() {
		return accountIdx;
	}


	public void setAccountIdx(int accountIdx) {
		this.accountIdx = accountIdx;
	}


	public int getParentIdx() {
		return parentIdx;
	}


	public void setParentIdx(int parentIdx) {
		this.parentIdx = parentIdx;
	}


	public String getCreatedBy() {
		return createdBy;
	}


	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}


	public Date getCreateDate() {
		return createDate;
	}


	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


	public String getModifiedBy() {
		return modifiedBy;
	}


	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}


	public Date getModifiedDate() {
		return modifiedDate;
	}


	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
