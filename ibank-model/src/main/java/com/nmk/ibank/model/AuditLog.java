package com.nmk.ibank.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessorOrder;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement
@XmlAccessorOrder
public class AuditLog implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private Integer idx;
	private String description;
	private String xmlMsg;
	private int transTypeIdx;
	private String createdBy;
	private Date createDate;
	private String modifiedBy;
	private Date modifiedDate;
	

	public AuditLog() {
	}

/*	public AuditLog(int transTypeIdx,String createdBy, Date createDate) {
		this.transTypeIdx = transTypeIdx;
		this.createdBy = createdBy;
		this.createDate = createDate;
		
	}*/
	

	public AuditLog(String description, String xmlMsg, int transTypeIdx ,String createdBy, Date createDate, String modifiedBy,
			Date modifiedDate) {
		this.description = description;
		this.xmlMsg = xmlMsg;
		this.transTypeIdx = transTypeIdx;
		this.createdBy = createdBy;
		this.createDate = createDate;
		this.modifiedBy = modifiedBy;
		this.modifiedDate = modifiedDate;
	
	}


	public Integer getIdx() {
		return idx;
	}

	public void setIdx(Integer idx) {
		this.idx = idx;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getXmlMsg() {
		return xmlMsg;
	}

	public void setXmlMsg(String xmlMsg) {
		this.xmlMsg = xmlMsg;
	}

	public int getTransTypeIdx() {
		return transTypeIdx;
	}

	public void setTransTypeIdx(int transTypeIdx) {
		this.transTypeIdx = transTypeIdx;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}



}
