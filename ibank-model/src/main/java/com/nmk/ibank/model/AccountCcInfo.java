package com.nmk.ibank.model;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessorOrder;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorOrder
public class AccountCcInfo implements Serializable {

	private int idx;

	private String cardNumber;
	private String cvvNo;
	private String cardIssueDate;
	private String cardExpDate;
	private String createdBy;
	private Date createDate;
	private String modifiedBy;
	private Date modifiedDate;
	private int ibankAccountIdx;

	public AccountCcInfo() {
		super();
	}

	public AccountCcInfo(String cardNumber, String cvvNo, String cardIssueDate, String cardExpDate, String createdBy,
			Date createDate, String modifiedBy, Date modifiedDate, int ibankAccountIdx) {
		super();
		this.cardNumber = cardNumber;
		this.cvvNo = cvvNo;
		this.cardIssueDate = cardIssueDate;
		this.cardExpDate = cardExpDate;
		this.createdBy = createdBy;
		this.createDate = createDate;
		this.modifiedBy = modifiedBy;
		this.modifiedDate = modifiedDate;
		this.ibankAccountIdx = ibankAccountIdx;
	}

	public int getIdx() {
		return idx;
	}

	public void setIdx(int idx) {
		this.idx = idx;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getCvvNo() {
		return cvvNo;
	}

	public void setCvvNo(String cvvNo) {
		this.cvvNo = cvvNo;
	}

	public String getCardIssueDate() {
		return cardIssueDate;
	}

	public void setCardIssueDate(String cardIssueDate) {
		this.cardIssueDate = cardIssueDate;
	}

	public String getCardExpDate() {
		return cardExpDate;
	}

	public void setCardExpDate(String cardExpDate) {
		this.cardExpDate = cardExpDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public int getIbankAccountIdx() {
		return ibankAccountIdx;
	}

	public void setIbankAccountIdx(int ibankAccountIdx) {
		this.ibankAccountIdx = ibankAccountIdx;
	}

}
