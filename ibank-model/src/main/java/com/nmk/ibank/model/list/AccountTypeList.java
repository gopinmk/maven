package com.nmk.ibank.model.list;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.nmk.ibank.model.AccountType;
import com.nmk.ibank.model.AuditLog;

@XmlRootElement(name = "accountType")
public class AccountTypeList extends DataList<AccountType> {
	@XmlElement(name = "accountType")
	@JsonProperty(value="accountType")
	public List<AccountType> getData() {
		return data;	
	}

}
