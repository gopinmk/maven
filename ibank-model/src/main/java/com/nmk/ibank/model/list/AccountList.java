package com.nmk.ibank.model.list;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.nmk.ibank.model.Account;

@XmlRootElement(name = "Account")
public class AccountList extends DataList<Account> {

	@XmlElement(name = "Account")
	@JsonProperty(value="Account")
	public List<Account> getData() {
		return data;	
	}

	
}
