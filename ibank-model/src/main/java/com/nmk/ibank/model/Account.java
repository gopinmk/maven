package com.nmk.ibank.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessorOrder;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorOrder
public class Account implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	private int idx;
	private String name;
	private String description;
	private Date openDate;
	private Date closedDate;
	private int locationIdx;
	private int userUId;
	private String createdBy;
	private Date createDate;
	private String modifiedBy;
	private Date modifiedDate;
	private int ibankAccountTypeIdx;
	public Account() {
		super();
	}
	public Account(String name,String description, Date openDate, Date closedDate, int locationIdx, int userUId, String createdBy,
			Date createDate, String modifiedBy, Date modifiedDate, int ibankAccountTypeIdx) {
		super();
		this.name = name;
		this.description=description;
		this.openDate = openDate;
		this.closedDate = closedDate;
		this.locationIdx = locationIdx;
		this.userUId = userUId;
		this.createdBy = createdBy;
		this.createDate = createDate;
		this.modifiedBy = modifiedBy;
		this.modifiedDate = modifiedDate;
		this.ibankAccountTypeIdx = ibankAccountTypeIdx;
	}
	public int getIdx() {
		return idx;
	}
	public void setIdx(int idx) {
		this.idx = idx;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getOpenDate() {
		return openDate;
	}
	public void setOpenDate(Date openDate) {
		this.openDate = openDate;
	}
	public Date getClosedDate() {
		return closedDate;
	}
	public void setClosedDate(Date closedDate) {
		this.closedDate = closedDate;
	}
	public int getLocationIdx() {
		return locationIdx;
	}
	public void setLocationIdx(int locationIdx) {
		this.locationIdx = locationIdx;
	}
	public int getUserUId() {
		return userUId;
	}
	public void setUserUId(int userUId) {
		this.userUId = userUId;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public int getIbankAccountTypeIdx() {
		return ibankAccountTypeIdx;
	}
	public void setIbankAccountTypeIdx(int ibankAccountTypeIdx) {
		this.ibankAccountTypeIdx = ibankAccountTypeIdx;
	}
	




}
