package com.nmk.ibank.model.list;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.nmk.ibank.model.Location;
import com.nmk.ibank.model.Transaction;

@XmlRootElement(name="locations")
public class LocationList extends DataList<Location>{

	@XmlElement(name= "locations")
	@JsonProperty(value = "locations")
	
	public List<Location> getData() {
		return data;	
	}
	
}
