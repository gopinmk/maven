package com.nmk.ibank.model.list;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.nmk.ibank.model.Transaction;

@XmlRootElement(name = "transaction")
public class TransactionList extends DataList<Transaction> { 

		@XmlElement(name = "transaction")
		@JsonProperty(value="transaction")
		public List<Transaction> getData() {
			return data;	
		}

}
