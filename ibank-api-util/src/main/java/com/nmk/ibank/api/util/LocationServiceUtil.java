package com.nmk.ibank.api.util;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;

import com.nmk.ibank.model.AuditLog;
import com.nmk.ibank.model.Location;
import com.nmk.ibank.service.entity.IbankAuditLog;
import com.nmk.ibank.service.entity.IbankLocation;

public class LocationServiceUtil {

	public static Location convertibankLocationObjectToLocation(IbankLocation ibankLocation) {
		Location location = new Location();
		try {
			BeanUtils.copyProperties(location, ibankLocation);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new RuntimeException(e);
		}
		return location;
	}

	public static IbankLocation convertLocationToibankLocation(Location location) {
		IbankLocation ibankLocation = new IbankLocation();
		try {
			BeanUtils.copyProperties(ibankLocation, location);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new RuntimeException(e);
		}
		return ibankLocation;
	}

	public static List<Location> convertibankLocationListToLocation(List<IbankLocation> ibankLocationList) {
		List<Location> locationList = new ArrayList<Location>();
		try {
			for (IbankLocation ibankLocation : ibankLocationList) {
				Location location = new Location();
				BeanUtils.copyProperties(location, ibankLocation);
				locationList.add(location);
			}
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new RuntimeException(e);
		}
		return locationList;
	}

}
