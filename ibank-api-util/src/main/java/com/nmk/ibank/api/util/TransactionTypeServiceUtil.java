package com.nmk.ibank.api.util;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;

import com.nmk.ibank.model.TransactionType;
import com.nmk.ibank.service.entity.IbankTransactionType;

public class TransactionTypeServiceUtil {
	
	public static  TransactionType convertibankTransactionTypeObjectToTransactionType(IbankTransactionType ibanktransactionType) {
		 TransactionType transactionType = new TransactionType();
		try {
			BeanUtils.copyProperties(transactionType, ibanktransactionType);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new RuntimeException(e);
		}
		return transactionType;
	}

	public static IbankTransactionType convertTransactionTypeToibankTransactionType(TransactionType transactionType) {
		IbankTransactionType ibankTransactionType = new IbankTransactionType();
		try {
			BeanUtils.copyProperties(ibankTransactionType, transactionType);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new RuntimeException(e);
		}
		return ibankTransactionType;
	}

	public static List<TransactionType> convertibankAuditLogListToAuditLog(List<IbankTransactionType> ibankTransactionTypeList) {
		List<TransactionType> transactionTypeList = new ArrayList<TransactionType>();
		try {
			for (IbankTransactionType ibankTransactionType : ibankTransactionTypeList) {
				TransactionType transactionType = new TransactionType();
				BeanUtils.copyProperties(transactionType, ibankTransactionType);
				transactionTypeList.add(transactionType);
			}
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new RuntimeException(e);
		}
		return transactionTypeList;
	}
}
