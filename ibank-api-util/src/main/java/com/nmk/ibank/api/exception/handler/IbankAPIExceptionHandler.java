package com.nmk.ibank.api.exception.handler;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import com.nmk.ibank.api.exception.HTTP400Exception;
import com.nmk.ibank.api.exception.HTTP404Exception;
import com.nmk.ibank.api.model.RESTAPIErrorInfo;

@ControllerAdvice
@RestController
public class IbankAPIExceptionHandler {
	
	
	protected final Logger log = Logger.getLogger(this.getClass());
	protected ApplicationEventPublisher eventPublisher;
	protected static final String DEFAULT_PAGE_SIZE = "100";
	protected static final String DEFAULT_PAGE_NUM = "0";

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(HTTP400Exception.class)
	public @ResponseBody RESTAPIErrorInfo handleDataStoreException(HTTP400Exception ex, WebRequest request,
			HttpServletResponse response) {
		log.info("Converting Data Store exception to RestResponse : " + ex.getMessage());
		// counterService.increment("com.rollingstone.http.400.count");
		return new RESTAPIErrorInfo(ex, "You messed up.");
	}

	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(HTTP404Exception.class)
	public @ResponseBody RESTAPIErrorInfo handleResourceNotFoundException(HTTP404Exception ex) {
		log.info("ResourceNotFoundException handler:" + ex.getMessage());
		// counterService.increment("com.rollingstone.http.404.count");
		return new RESTAPIErrorInfo(ex, "Sorry I couldn't find it.");
	}

	public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
		this.eventPublisher = applicationEventPublisher;
	}

	public static <T> T checkResourceFound(final T resource) {
		if (resource == null) {
			throw new HTTP404Exception("resource not found");
		}
		return resource;
	}
}
