package com.nmk.ecomm.api.exception.handler;

import java.util.Date;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import com.nmk.ecomm.api.exception.HTTP404NotFoundException;
import com.nmk.ecomm.api.model.ErrorDetail;

@ControllerAdvice
@RestController
public class ECommAPIExceptionHandler {
	@ExceptionHandler(Exception.class)
	  public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
	    ErrorDetail errorDetails = new ErrorDetail(HttpStatus.BAD_REQUEST.value(), new Date(), ex.getMessage(),
	        request.getDescription(false));
	    return new ResponseEntity(errorDetails, HttpStatus.BAD_REQUEST);
	  }
	
	@ExceptionHandler(HTTP404NotFoundException.class)
	  public final ResponseEntity<Object> handleNotFoundException(HTTP404NotFoundException ex, WebRequest request) {
	    ErrorDetail errorDetails = new ErrorDetail(HttpStatus.NOT_FOUND.value(), new Date(), ex.getMessage(),
	        request.getDescription(false));
	    return new ResponseEntity(errorDetails, HttpStatus.NOT_FOUND);
	  }
	
	  protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
	      HttpHeaders headers, HttpStatus status, WebRequest request) {
	    ErrorDetail errorDetails = new ErrorDetail(HttpStatus.BAD_REQUEST.value(), new Date(), "Validation Failed",
	        ex.getBindingResult().toString());
	    return new ResponseEntity(errorDetails, HttpStatus.BAD_REQUEST);
	  } 
}
