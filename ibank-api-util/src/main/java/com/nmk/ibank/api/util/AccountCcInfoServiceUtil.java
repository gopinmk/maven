package com.nmk.ibank.api.util;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;

import com.nmk.ibank.model.AccountCcInfo;
import com.nmk.ibank.service.entity.IbankAccountCcInfo;

public class AccountCcInfoServiceUtil {
	public static  AccountCcInfo convertibankAccountCcInfoObjectToAccountCcInfo(IbankAccountCcInfo ibankAccountCcInfo){
		AccountCcInfo accountCcInfo = new AccountCcInfo();
		try {
			BeanUtils.copyProperties(accountCcInfo, ibankAccountCcInfo);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new RuntimeException(e);
		}
		return accountCcInfo;
	}
	public static IbankAccountCcInfo convertAccountCcInfoToibankAccountCcInfo(AccountCcInfo accountCcInfo){
		IbankAccountCcInfo ibankAccountCcInfo = new IbankAccountCcInfo();
		try {
			BeanUtils.copyProperties(ibankAccountCcInfo, accountCcInfo);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new RuntimeException(e);
		}
		return ibankAccountCcInfo;
	}
	public static List<AccountCcInfo> convertibankAccountCcInfoListToAccountCcInfo(List<IbankAccountCcInfo> ibankAccountCcInfoList){
		List<AccountCcInfo> AccountCcInfoList = new ArrayList<AccountCcInfo>();
		try {
		     for (IbankAccountCcInfo ibankAccountCcInfo: ibankAccountCcInfoList ) {
		    	 AccountCcInfo accountCcInfo= new AccountCcInfo();
		         BeanUtils.copyProperties(accountCcInfo, ibankAccountCcInfo);
		         AccountCcInfoList.add(accountCcInfo);
		      }
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new RuntimeException(e);
		}
		return AccountCcInfoList;
	}
}
