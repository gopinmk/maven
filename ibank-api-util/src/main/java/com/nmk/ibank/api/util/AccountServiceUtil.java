package com.nmk.ibank.api.util;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;

import com.nmk.ibank.model.Account;
import com.nmk.ibank.model.AuditLog;
import com.nmk.ibank.service.entity.IbankAccount;
import com.nmk.ibank.service.entity.IbankAuditLog;

public class AccountServiceUtil {

	public static Account convertibankAccountObjectToAccount(IbankAccount ibankAccount) {
		Account account = new Account();
		try {

			BeanUtils.copyProperties(account, ibankAccount);
		} catch (IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
			new RuntimeException(e);
		}

		return account;
	}

	public static IbankAccount convertAccountToibankAccount(Account account) {

		IbankAccount ibankAccount = new IbankAccount();

		try {
			BeanUtils.copyProperties(ibankAccount, account);

		} catch (IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
			new RuntimeException(e);
		}

		return ibankAccount;

	}

	public static List<Account> convertibankAccountListToAccount(List<IbankAccount> ibankAccountList){
		List<Account> accountList = new ArrayList<Account>();
		try {
		     for (IbankAccount ibankAccount: ibankAccountList ) {
		    	 Account account= new Account();
		         BeanUtils.copyProperties(account, ibankAccount);
		         accountList.add(account);
		      }
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new RuntimeException(e);
		}
		return accountList;
	}
}
