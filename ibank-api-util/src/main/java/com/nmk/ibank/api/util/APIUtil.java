package com.nmk.ibank.api.util;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;

public class APIUtil<Entity, Domain> {
	public Domain convertEntitytoDomain(Entity e) {
		Domain d = null;
		try {
			BeanUtils.copyProperties(d, e);
		} catch (IllegalAccessException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
			new RuntimeException(ex);
		} catch (InvocationTargetException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
			new RuntimeException(ex);
		}
		return d;
	}

	public Entity convertDomaintoEntity(Domain d) {
		Entity e = null;
		try {
			BeanUtils.copyProperties(e, d);
		} catch (IllegalAccessException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
			new RuntimeException(ex);
		} catch (InvocationTargetException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
			new RuntimeException(ex);
		}
		return e;
	}

	public List<Domain> convertEntityListToUser(List<Entity> entityList) {
		List<Domain> domainList = new ArrayList<Domain>();
		try {
			for (Entity e : entityList) {
				Domain d = null;
				BeanUtils.copyProperties(d, e);
				domainList.add(d);
			}
		} catch (IllegalAccessException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
			new RuntimeException(ex);
		} catch (InvocationTargetException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
			new RuntimeException(ex);
		}
		return domainList;
	}
}
