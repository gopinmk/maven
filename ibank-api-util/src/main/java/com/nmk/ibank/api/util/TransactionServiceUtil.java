package com.nmk.ibank.api.util;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;

import com.nmk.ibank.model.Transaction;
import com.nmk.ibank.service.entity.IbankTransaction;

public class TransactionServiceUtil {
	public static Transaction convertIbankTransactionObjectToTransaction(IbankTransaction ibankTransaction) {
		Transaction transaction = new Transaction();
		try {
			BeanUtils.copyProperties(transaction, ibankTransaction);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new RuntimeException(e);
		}
		return transaction;
	}

	public static IbankTransaction convertTransactionToIbankTransaction(Transaction transaction) {
		IbankTransaction ibankTransaction = new IbankTransaction();
		try {
			BeanUtils.copyProperties(ibankTransaction, transaction);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new RuntimeException(e);
		}
		return ibankTransaction;
	}

	public static List<Transaction> convertIbankTransactionListToTransaction(List<IbankTransaction> ibankTransactionList){
		List<Transaction> transactionList = new ArrayList<Transaction>();
		try {
		     for (IbankTransaction ibankTransaction: ibankTransactionList ) {
		    	 Transaction transaction= new Transaction();
		         BeanUtils.copyProperties(transaction, ibankTransaction);
		         transactionList.add(transaction);
		      }
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new RuntimeException(e);
		}
		return transactionList;
	}

}
