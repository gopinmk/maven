package com.nmk.ibank.api.util;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;

import com.nmk.ibank.model.AuditLog;
import com.nmk.ibank.service.entity.IbankAuditLog;

public class AuditLogServiceUtil {
	public static AuditLog convertibankAuditLogObjectToAuditLog(IbankAuditLog ibankAuditLog){
		AuditLog auditlog = new AuditLog();
		try {
			BeanUtils.copyProperties(auditlog, ibankAuditLog);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new RuntimeException(e);
		}
		return auditlog;
	}
	public static IbankAuditLog convertAuditLogToibankAuditLog(AuditLog auditlog){
		IbankAuditLog ibankAuditLog = new IbankAuditLog();
		try {
			BeanUtils.copyProperties(ibankAuditLog, auditlog);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new RuntimeException(e);
		}
		return ibankAuditLog;
	}
	public static List<AuditLog> convertibankAuditLogListToAuditLog(List<IbankAuditLog> ibankAuditLogList){
		List<AuditLog> auditlogList = new ArrayList<AuditLog>();
		try {
		     for (IbankAuditLog ibankAuditLog: ibankAuditLogList ) {
		         AuditLog auditlog= new AuditLog();
		         BeanUtils.copyProperties(auditlog, ibankAuditLog);
		         auditlogList.add(auditlog);
		      }
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new RuntimeException(e);
		}
		return auditlogList;
	}
}
