package com.nmk.api.account.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nmk.api.account.repository.AccountRepository;
import com.nmk.ibank.api.exception.HTTP404Exception;
import com.nmk.ibank.api.util.AccountServiceUtil;
import com.nmk.ibank.api.util.AuditLogServiceUtil;
import com.nmk.ibank.model.Account;
import com.nmk.ibank.model.AuditLog;
import com.nmk.ibank.model.list.AccountList;
import com.nmk.ibank.model.list.AuditLogList;
import com.nmk.ibank.service.entity.IbankAccount;
import com.nmk.ibank.service.entity.IbankAuditLog;

@RestController
@RequestMapping("/api/v1/account")
public class AccountController {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	AccountRepository accountRepository;

	@RequestMapping("/")
	String hello() {
		logger.debug("Debug message");
		logger.info("Info message");
		logger.warn("Warn message");
		logger.error("Error message");
		return "Done";
	}

	@GetMapping(value = "", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public AccountList getAllDetails() {
		List<IbankAccount> ibankAccountList = accountRepository.findAll();
		List<Account> list = AccountServiceUtil.convertibankAccountListToAccount(ibankAccountList);
		AccountList accountList = new AccountList();
		accountList.setData(list);
		return accountList;
	}

	@GetMapping(value = "/{idx}", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public Account getAccountByIdx(@PathVariable(value = "idx") Integer accountidx) {
		IbankAccount ibankAccount = accountRepository.findById(accountidx).orElseThrow(() -> new HTTP404Exception("idx-" + accountidx + " is not exist"));

		return AccountServiceUtil.convertibankAccountObjectToAccount(ibankAccount);
	}

	@PostMapping(value = "", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public Account createAccount(@Valid @RequestBody Account account) {
		
		IbankAccount ibankAccount = AccountServiceUtil.convertAccountToibankAccount(account);
		IbankAccount ibankAccount1 = accountRepository.save(ibankAccount);
		return AccountServiceUtil.convertibankAccountObjectToAccount(ibankAccount1);
	}
	
	@DeleteMapping(value = "/{idx}", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> deleteAccount(@PathVariable(value = "idx") Integer accountidx) {
		IbankAccount ibankAccount = accountRepository.findById(accountidx)
				.orElseThrow(() -> new HTTP404Exception("idx-" + accountidx + " is not exist"));
		accountRepository.delete(ibankAccount);

		return ResponseEntity.ok().build();

	}

	@PutMapping(value="/{idx}",produces= {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
	public Account updateBrand(@PathVariable(value="idx") Integer accountidx , @Valid @RequestBody Account account) {
		
		IbankAccount ibankAccount = accountRepository.findById(accountidx).orElseThrow(() -> new HTTP404Exception("idx-" + accountidx + " is not exist"));

		
		ibankAccount.setName(account.getName());
		ibankAccount.setDescription(account.getDescription());
		ibankAccount.setOpenDate(account.getOpenDate());
		ibankAccount.setClosedDate(account.getClosedDate());
		ibankAccount.setLocationIdx(account.getLocationIdx());
		ibankAccount.setUserUId(account.getUserUId());
		ibankAccount.setCreatedBy(account.getCreatedBy());
		ibankAccount.setCreateDate(account.getCreateDate());
		ibankAccount.setModifiedBy(account.getModifiedBy());
		ibankAccount.setModifiedDate(account.getModifiedDate());
		
		IbankAccount updateAccount = accountRepository.save(ibankAccount);
		
		return AccountServiceUtil.convertibankAccountObjectToAccount(updateAccount);

				
	}
	
	

}
