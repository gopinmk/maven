package com.nmk.api.account.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nmk.ibank.service.entity.IbankAccount;
import com.zaxxer.hikari.util.ConcurrentBag.IBagStateListener;

public interface AccountRepository extends JpaRepository<IbankAccount, Integer> {

}
