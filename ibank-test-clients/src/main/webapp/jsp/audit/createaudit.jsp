<%@include file="header.html"%>
<html>
<head>
<title>Audit Dashboard</title>
</head>

<body bgcolor="pink" leftmargin="10px" rightmargin="10px">

	<div align="center">
		<h2>Create Audit</h2>
		<!--Body: Delete Category information-->
		<form method="post"
			action="/ibank-test-clients/createaudit.do">
			<table cellpadding="0" cellspacing="0" width="50%"">
			 	<tr>
					<td bgcolor="#FAFAF9" style="padding-left: 10px;">Audit Idx</td>
					<td><input type="text" name="idx" maxlength="10" value="" /></td>
				</tr> 
				<tr>
					<td bgcolor="#FAFAF9" style="padding-left: 10px;">Audit Description</td>
					<td><input type="text" name="description" maxlength="10" value="" /></td>
				</tr>
				<tr>
					<td bgcolor="#FAFAF9" style="padding-left: 10px;">Audit XmL</td>
					<td><input type="text" name="xmlMsg" maxlength="10" value="" /></td>
				</tr>
				<tr>
					<td bgcolor="#FAFAF9" style="padding-left: 10px;">Audit CreatedBy</td>
					<td><input type="text" name="createdBy" maxlength="10" value="" /></td>
				</tr>
			 	<tr>
					<td bgcolor="#FAFAF9" style="padding-left: 10px;">Audit modifiedBy</td>
					<td><input type="text" name="modifiedBy" maxlength="10" value="" /></td>
				</tr>
				<!-- <tr>
					<td bgcolor="#FAFAF9" style="padding-left: 10px;">Audit modifiedDate</td>
					<td><input type="text" name="modifiedDate" maxlength="10" value="" /></td>
				</tr>   -->
				<tr>
					<td bgcolor="#FAFAF9" style="padding-left: 10px;">Audit transTypeIdx</td>
					<td><input type="text" name="transTypeIdx" maxlength="10" value="" /></td>
				</tr>
				<tr>
					<td colspan="2" align="right"><br>
						<table width="50%">
							<tr>
								<td><input type="submit" name="create" value="Create" /></td>
								<td width="201"><input type="reset" name="clear" value="Clear" />&nbsp;</td>
							</tr>
						</table></td>
				</tr>
			</table>
		</form>
	</div>
</body>

</html>
<%@include file="footer.html"%>
</body>
