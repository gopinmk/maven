package com.nmk.ibank.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

// don't use this. This is not sprint boot application. Added for keeping spring boot happy.
@SpringBootApplication
public class IbankSpringMainApp {
	public static void main(String[] args) {
		SpringApplication.run(IbankSpringMainApp.class, args);
	}
}
