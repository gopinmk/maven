package com.nmk.ibank.web.controller;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


import com.nmk.ibank.web.service.intf.IbankAuditlogService;
import com.nmk.ibank.web.vo.AuditLogVo;



@Controller
@Configuration
@ComponentScan("com.nmk.ibank.web.service.impl")
public class AuditWebController {
	private static final Logger logger = Logger.getLogger(AuditWebController.class);
	@Autowired
	IbankAuditlogService eRetailAuditService=null;
	@RequestMapping("/createaudit.do")
	public String addaudit(@ModelAttribute("AuditLogVo") AuditLogVo AuditLogVo) {
		if (logger.isDebugEnabled()) logger.debug("start: AuditWebController.addaudit()");
		if (logger.isTraceEnabled()) logger.trace("Audit id received from client: " + AuditLogVo );
		Date dt = new Date();
		AuditLogVo.setCreatedBy("user");
		//AuditLogVo.setIdx(22);
		//AuditLogVo.setTransTypeIdx(5);
		//AuditLogVo.setDescription("user");
		//AuditLogVo.setModifiedBy("vijay");
		//AuditLogVo.setCreatedBy("user");
		AuditLogVo.setCreateDate(dt);
		AuditLogVo.setModifiedDate(dt);
		eRetailAuditService.createAuditlog(AuditLogVo);
		if (logger.isTraceEnabled()) logger.trace("Audit Information received from service: \n" + AuditLogVo);
		if (logger.isDebugEnabled()) logger.debug("End: AuditWebController.addaudit()");
		return "audit/createauditsuccess";
	}
	@RequestMapping("/audit.do")
	public String displayAudit(@RequestParam("auditId") String auditId, Model model) {
	if (logger.isDebugEnabled()) logger.debug("start: AuditWebController.displayAudit()");
	if (logger.isTraceEnabled()) logger.trace("Audit id received from client: " + auditId);
	AuditLogVo AuditLogVo = eRetailAuditService.getAuditlogByIdx(Integer.parseInt(auditId));
	if (logger.isTraceEnabled()) logger.trace("Audit Information received from service: \n" + AuditLogVo);
	if (logger.isDebugEnabled()) logger.debug("End: AuditWebController.displayAudit()");
	//TransTypeVO transTypeVO2 = new TransTypeVO("Card", "Visa", "admin", new java.util.Date(), "admin", new java.util.Date());
	model.addAttribute("audit", AuditLogVo);
	return "audit/viewauditsuccess.jsp";
	}
    @RequestMapping("/updateAudit.do")
	public String updateBrand(@ModelAttribute("AuditLogVo") AuditLogVo AuditLogVo) {
		if (logger.isDebugEnabled()) logger.debug("start: AuditWebController.updateBrand()");
		if (logger.isTraceEnabled()) logger.trace("Audit details received from client: \n"+AuditLogVo);
		Date dt = new Date();
		AuditLogVo.setCreatedBy("user");
		AuditLogVo.setCreateDate(dt);
		eRetailAuditService.updateAuditlog(AuditLogVo);
		if (logger.isDebugEnabled()) logger.debug("End: AuditWebController.updateBrand()");
		if (logger.isTraceEnabled()) logger.trace("Audit details received from client: \n"+AuditLogVo);
		return "audit/modifyauditsuccess";
	}

	@RequestMapping("/deleteAudit.do")
	public String deleteAuditlog(@RequestParam("auditId") String auditId) {
		if (logger.isDebugEnabled()) logger.debug("start: AuditWebController.deleteAuditlog()");
		if (logger.isTraceEnabled()) logger.trace("Audit details received from client: \n"+auditId);
		eRetailAuditService.deleteAuditlog(Integer.parseInt(auditId));
		if (logger.isDebugEnabled()) logger.debug("End: AuditWebController.deleteAuditlog()");
		if (logger.isTraceEnabled()) logger.trace("Audit details received from client: \n"+auditId);
		return "audit/deleteauditsuccess";
	}

	/*@RequestMapping("/getallaudits.do")
	public String getAllAudits(Model model) {
		if (logger.isDebugEnabled()) logger.debug("start: AuditWebController.getallaudits()");
		if (logger.isTraceEnabled()) logger.trace("AuditLog details received from client: \n");
		List<AuditLogVo> auditLog=eRetailAuditService.getAllAuditLog();
		if (logger.isDebugEnabled()) logger.debug("End: AuditWebController.getallaudits()");
		List<AuditLogVo> auditLogs = new ArrayList<>();
		model.addAttribute("auditLog", auditLogs);
		return "audit/displayallaudits";
	}*/
}
