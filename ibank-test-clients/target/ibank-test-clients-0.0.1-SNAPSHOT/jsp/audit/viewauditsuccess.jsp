<%@include file="header.html"%>
<html>
<head>
<title>Audit Dashboard</title>
</head>

<body bgcolor="pink" leftmargin="10px" rightmargin="10px">

	<div align="center">
		<h2>View Audit Success</h2>
		<table cellpadding="0" cellspacing="0" width="50%"">
			<tr>
				<td bgcolor="#FAFAF9" style="padding-left: 10px;">Audit Idx</td>
				<td bgcolor="#FAFAF9" style="padding-left: 10px;">${audit.idx}</td>
			</tr>
			<tr>
				<td bgcolor="#FAFAF9" style="padding-left: 10px;">Audit XmlMsg</td>
				<td bgcolor="#FAFAF9" style="padding-left: 10px;">${audit.xmlMsg}</td>
			</tr>
			<tr>
				<td bgcolor="#FAFAF9" style="padding-left: 10px;">Audit Description</td>
				<td bgcolor="#FAFAF9" style="padding-left: 10px;">${audit.description}</td>
			</tr>
		</table>
	</div>
</body>

</html>
<%@include file="footer.html"%>

