package com.nmk.api.transaction.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nmk.ibank.service.entity.IbankTransactionType;

public interface TransactionTypeRepository  extends JpaRepository<IbankTransactionType, Integer> {

}
