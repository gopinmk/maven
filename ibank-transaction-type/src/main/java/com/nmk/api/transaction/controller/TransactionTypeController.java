package com.nmk.api.transaction.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nmk.api.transaction.repository.TransactionTypeRepository;
import com.nmk.ibank.api.exception.HTTP404Exception;
import com.nmk.ibank.api.util.TransactionTypeServiceUtil;
import com.nmk.ibank.model.TransactionType;
import com.nmk.ibank.model.list.TransactionTypeList;
import com.nmk.ibank.service.entity.IbankTransactionType;

@RestController
@RequestMapping("/api/v1/transactiontype")
public class TransactionTypeController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	TransactionTypeRepository transactionTypeRepository;

	@RequestMapping("/")
	String hello() {
		logger.debug("Debug message");
		logger.info("Info message");
		logger.warn("Warn message");
		logger.error("Error message");
		return "Done";
	}

	@GetMapping(value = "", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public TransactionTypeList getAllDetails() {
		List<IbankTransactionType> ibankTransactionTypeList = transactionTypeRepository.findAll();
		List<TransactionType> list = TransactionTypeServiceUtil.convertibankAuditLogListToAuditLog(ibankTransactionTypeList);
		TransactionTypeList transactionTypeList = new TransactionTypeList();
		transactionTypeList.setData(list);
		return transactionTypeList;
	}

	@GetMapping(value = "/{idx}", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public TransactionType getAccountTypeByIdx(@PathVariable(value = "idx") Integer TransactionTypeIdx) {
		IbankTransactionType ibankTransactionType = transactionTypeRepository.findById(TransactionTypeIdx).orElseThrow(() -> new HTTP404Exception("idx-" + TransactionTypeIdx + " is not exist"));
		return TransactionTypeServiceUtil.convertibankTransactionTypeObjectToTransactionType(ibankTransactionType);
	}

	@PostMapping(value = "", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public TransactionType createTransactionType(@Valid @RequestBody TransactionType transactionType) {
		IbankTransactionType ibankTransactionType = TransactionTypeServiceUtil.convertTransactionTypeToibankTransactionType(transactionType);
		IbankTransactionType ibankTransactionType1 = transactionTypeRepository.save(ibankTransactionType);
		return TransactionTypeServiceUtil.convertibankTransactionTypeObjectToTransactionType(ibankTransactionType1);
	}

	@DeleteMapping(value = "/{idx}", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> deleteTransactionType(@PathVariable(value = "idx") Integer TransactionTypeIdx) {
		IbankTransactionType ibankAccountType = transactionTypeRepository.findById(TransactionTypeIdx)
				.orElseThrow(() -> new HTTP404Exception("idx-" + TransactionTypeIdx + " is not exist"));
		transactionTypeRepository.delete(ibankAccountType);
		return ResponseEntity.ok().build();
	}

	@PutMapping(value = "/{idx}", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public TransactionType updateAuditlog(@PathVariable(value = "idx") Integer accountTypeIdx,
			@Valid @RequestBody TransactionType transactionType) {
		IbankTransactionType ibankTransactionType = transactionTypeRepository.findById(accountTypeIdx)
				.orElseThrow(() -> new HTTP404Exception("idx-" + accountTypeIdx + " is not exist"));

		ibankTransactionType.setTransactionType(transactionType.getTransactionType());
		ibankTransactionType.setDescription(transactionType.getDescription());
		ibankTransactionType.setCreatedBy(transactionType.getModifiedBy());
		ibankTransactionType.setCreateDate(transactionType.getCreateDate());
		ibankTransactionType.setModifiedBy(transactionType.getModifiedBy());
		ibankTransactionType.setModifiedDate(transactionType.getModifiedDate());

		IbankTransactionType updateTransactionType = transactionTypeRepository.save(ibankTransactionType);

		return TransactionTypeServiceUtil.convertibankTransactionTypeObjectToTransactionType(updateTransactionType);
	}

}
