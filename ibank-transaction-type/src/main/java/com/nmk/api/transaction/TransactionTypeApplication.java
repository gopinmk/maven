package com.nmk.api.transaction;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EntityScan("com.nmk.ibank.service.entity")
@EnableJpaAuditing
public class TransactionTypeApplication {

	public static void main(String args[]) {
		SpringApplication.run(TransactionTypeApplication.class, args);
	}

}
