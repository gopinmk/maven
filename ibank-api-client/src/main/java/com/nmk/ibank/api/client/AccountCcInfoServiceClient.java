package com.nmk.ibank.api.client;

import java.util.Arrays;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.nmk.ibank.model.AccountCcInfo;

@RestController
public class AccountCcInfoServiceClient {
	@Autowired
	static RestTemplate restTemplate = new RestTemplate();
	
	static String AccountCcInfoSeriviceURL = "http://localhost:8081//api/v1/accountccinfo";

	public static AccountCcInfo getAccountCcByIdx(int id) {
		AccountCcInfo accountCcInfo = restTemplate.getForObject(AccountCcInfoSeriviceURL + "/" + id, AccountCcInfo.class);
		System.out.println(accountCcInfo.getIdx() + " " + accountCcInfo.getCardNumber() + " " + accountCcInfo.getCvvNo() + " "
				+ accountCcInfo.getCardIssueDate() + " " + accountCcInfo.getCardExpDate() + " " + accountCcInfo.getCreateDate() + " "
				+ accountCcInfo.getModifiedBy() + " " + accountCcInfo.getModifiedDate());
		return accountCcInfo;
	}

	public static AccountCcInfo getAllAccountCc() {
		RestTemplate restTemplate = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		ResponseEntity<String> result = restTemplate.exchange(AccountCcInfoSeriviceURL, HttpMethod.GET, entity,
				String.class);
		System.out.println(result);
		return null;
	}

	public static void deleteAccountCc(int id) {
		restTemplate.delete(AccountCcInfoSeriviceURL + "/" + id);

	}

	public static void createAccountCc(AccountCcInfo accountCcInfo) {
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<AccountCcInfo> request = new HttpEntity<AccountCcInfo>(accountCcInfo);
		ResponseEntity<AccountCcInfo> response = restTemplate.exchange(AccountCcInfoSeriviceURL, HttpMethod.POST, request,
				AccountCcInfo.class);
		if (200 == response.getStatusCodeValue())
			System.out.println("AuditLog Data Inserted Successfully");
	}

	public static void updateAccountCc(AccountCcInfo accountCcInfo) {
		AccountCcInfoSeriviceURL = "http://localhost:8081/api/v1/accountccinfo/1";
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<AccountCcInfo> request = new HttpEntity<AccountCcInfo>(accountCcInfo);
		ResponseEntity<AccountCcInfo> response = restTemplate.exchange(AccountCcInfoSeriviceURL, HttpMethod.PUT, request,
				AccountCcInfo.class);
		if (200 == response.getStatusCodeValue())
			System.out.println("AuditLog Data Updated Successfully");
	}

	public static void main(String[] args) {
		// We should use these for above methods
//	 AccountCcInfo accountCcInfo = new AccountCcInfo("1", "43343", "eafada", "afeafa", "kumar",
		//	new Date(), "raj", new Date(), 1);
	// updateAccountCc(accountCcInfo);
		// createAccountCc(accountCcInfo);
	//AccountCcInfo accountCcInfo = getAccountCcByIdx(3);
	//	 AccountCcInfo accountCcInfo =getAllAccountCc();
		// AccountCcInfo accountCcInfo = new AccountCcInfo();
		// deleteAccountCc(32);
	}
}


