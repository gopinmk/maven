package com.nmk.ibank.api.client;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.nmk.ibank.model.AuditLog;

@RestController
public class AuditlogServiceClient {
	@Autowired
	static RestTemplate restTemplate = new RestTemplate();
	static String auditlogSeriviceURL = "http://localhost:8081//api/v1/auditlog";

	public static AuditLog getAuditlogByIdx(int id) {
		AuditLog auditlog = restTemplate.getForObject(auditlogSeriviceURL + "/" + id, AuditLog.class);
		System.out.println(auditlog.getIdx() + " " + auditlog.getDescription() + " " + auditlog.getXmlMsg() + " "
				+ auditlog.getTransTypeIdx() + " " + auditlog.getCreatedBy() + " " + auditlog.getCreateDate() + " "
				+ auditlog.getModifiedBy() + " " + auditlog.getModifiedDate());
		return auditlog;
	}

	public static AuditLog getAllAuditLog() {
		RestTemplate restTemplate = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		ResponseEntity<String> result = restTemplate.exchange(auditlogSeriviceURL, HttpMethod.GET, entity,
				String.class);
		System.out.println(result);
		return null;
	}

	public static void deleteAuditlog(int id) {
		restTemplate.delete(auditlogSeriviceURL + "/" + id);

	}

	public static void createAuditlog(AuditLog auditlog) {
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<AuditLog> request = new HttpEntity<AuditLog>(auditlog);
		ResponseEntity<AuditLog> response = restTemplate.exchange(auditlogSeriviceURL, HttpMethod.POST, request,
				AuditLog.class);
		if (200 == response.getStatusCodeValue())
			System.out.println("AuditLog Data Inserted Successfully");
	}

	public static void updateAuditlog(AuditLog auditlog) {
		auditlogSeriviceURL = "http://localhost:8081//api/v1/auditlog/1";
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<AuditLog> request = new HttpEntity<AuditLog>(auditlog);
		ResponseEntity<AuditLog> response = restTemplate.exchange(auditlogSeriviceURL, HttpMethod.PUT, request,
				AuditLog.class);
		if (200 == response.getStatusCodeValue())
			System.out.println("AuditLog Data Updated Successfully");
	}

	public static void main(String[] args) {
		// We should use these for above methods
		// AuditLog auditlog = new AuditLog("Acer10", "Satish2", 255, "rajdgsg", new
		// java.util.Date(), "asdsaddgs", new java.util.Date());
		// updateBrand(auditlog);
		// createAuditlog(auditlog);
		// AuditLog auditlog = getAuditlogByIdx(33);
		// AuditLog auditlog =getAllAuditLog();
		// AuditLog auditlog = new AuditLog();
		// deleteAuditlog(32);
	}
}
