package com.nmk.ibank.api.client;

import java.util.Arrays;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.nmk.ibank.model.Account;
import com.nmk.ibank.model.AuditLog;

@RestController

public class AccountServiceClient {

	@Autowired

	static RestTemplate restTemplate = new RestTemplate();
	static String accountSeriviceURL = "http://localhost:8081/api/v1/account";

	public static Account getAccountByIdx(int id) {
		Account account = restTemplate.getForObject(accountSeriviceURL + "/" + id, Account.class);
		System.out.println(account.getName() + " " + account.getDescription() + " " + account.getOpenDate() + " "
				+ account.getClosedDate() + " " + account.getLocationIdx() + " " + account.getUserUId() + " "
				+ account.getCreatedBy() + " " + account.getCreateDate() + " " + account.getModifiedBy() + " "
				+ account.getModifiedDate());
		return account;
	}

	public static Account getAllAccount() {
		RestTemplate restTemplate = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		ResponseEntity<String> result = restTemplate.exchange(accountSeriviceURL, HttpMethod.GET, entity, String.class);
		System.out.println(result);
		return null;
	}

	public static void deleteAccount(int id) {
		restTemplate.delete(accountSeriviceURL + "/" + id);

	}

	public static void createAccount(Account account) {
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<Account> request = new HttpEntity<Account>(account);
		ResponseEntity<Account> response = restTemplate.exchange(accountSeriviceURL, HttpMethod.POST, request,
				Account.class);
		if (200 == response.getStatusCodeValue())
			System.out.println("Account Data Inserted Successfully");
	}

	public static void updateAccount(Account account) {
		accountSeriviceURL = "http://localhost:8081/api/v1/account/1";
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<Account> request = new HttpEntity<Account>(account);
		ResponseEntity<Account> response = restTemplate.exchange(accountSeriviceURL, HttpMethod.PUT, request,
				Account.class);
		if (200 == response.getStatusCodeValue())
			System.out.println("AuditLog Data Updated Successfully");
	}

	public static void main(String[] args) {
		// We should use these for above methods
	//	 Account account = new Account("auiud","hai", new Date(), new Date(), 5768, 12, "kumar",
//					new Date(), "raj", new Date(), 21);
		// updateAccount(account);
	//	 createAccount(account);
		// Account account = getAccountByIdx(1);
		 Account account =getAllAccount();
		// Account account = new Account();
		// deleteAuditlog(32);
	}
}
