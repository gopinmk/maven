package com.nmk.ibank.api.client;

import java.util.Arrays;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.nmk.ibank.model.Location;


@RestController
public class LocationServiceClient {

	@Autowired
	static RestTemplate restTemplate = new RestTemplate();
	static String locationSeriviceURL = "http://localhost:8081//api/v1/location";

	public static Location getLocationByIdx(int id) {
		Location location = restTemplate.getForObject(locationSeriviceURL + "/" + id, Location.class);
		System.out.println(location.getIdx() + " " + location.getName() + " " + location.getAddress1() + " "
				+ location.getAddress2() + " " +" " + location.getCity() + " "+" " + location.getState() + " "+" " + location.getZipcode() + " " + location.getCountry()+" " + location.getPhone() +" " + location.getFax() +" " + location.getCreatedBy() +" " + location.getCreateDate() + " "
				+ location.getModifiedBy() + " " + location.getModifiedDate());
		return location;
	}

	public static Location getAllLocation() {
		RestTemplate restTemplate = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		ResponseEntity<String> result = restTemplate.exchange(locationSeriviceURL, HttpMethod.GET, entity,
				String.class);
		System.out.println(result);
		return null;
	}

	public static void deleteLocation(int id) {
		restTemplate.delete(locationSeriviceURL + "/" + id);

	}

	public static void createLocation(Location location) {
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<Location> request = new HttpEntity<Location>(location);
		ResponseEntity<Location> response = restTemplate.exchange(locationSeriviceURL, HttpMethod.POST, request,
				Location.class);
		if (200 == response.getStatusCodeValue())
			System.out.println("AuditLog Data Inserted Successfully");
	}

	public static void updateLocation(Location location) {
		locationSeriviceURL = "http://localhost:8081//api/v1/location/1";
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<Location> request = new HttpEntity<Location>(location);
		ResponseEntity<Location> response = restTemplate.exchange(locationSeriviceURL, HttpMethod.PUT, request,
				Location.class);
		if (200 == response.getStatusCodeValue())
			System.out.println("AuditLog Data Updated Successfully");
	}

	public static void main(String[] args) {
		// We should use these for above methods
		// Location location = new Location("ksk", "one", "two", "hola", "dff", "dvce",
				//	"dsmf", "fdfd", "dafdas", "fda", new Date(), "dafsd",
					//new Date());
		// updateLocation(location);
		 //.createLocation(location);
		// Location location = getLocationByIdx(3);
	//	 Location location =getAllLocation();
		// Location location = new AuditLog();
	//	 deleteLocation(3);
	}
}
