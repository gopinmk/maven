package com.nmk.ibank.api.client;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.nmk.ibank.model.Transaction;

@RestController
public class TransactionServiceClient {

	@Autowired
	static RestTemplate restTemplate = new RestTemplate();
	static String transactionSeriviceURL = "http://localhost:8081/api/v1/transaction";

	public static Transaction gettransactionByIdx(int id) {
		Transaction transaction = restTemplate.getForObject(transactionSeriviceURL + "/" + id, Transaction.class);
		System.out.println(transaction.getIdx() + " " + transaction.getTransactionId() + " "
				+ transaction.getTransactionDate() + " " + transaction.getTransAmount() + " "
				+ transaction.getDescription() + " " + transaction.getAccountIdx() + " " + transaction.getParentIdx()
				+ " " + transaction.getCreatedBy() + " " + transaction.getCreateDate() + " "
				+ transaction.getModifiedBy() + " " + transaction.getModifiedDate());
		return transaction;
	}

	public static Transaction getAllTransaction() {
		RestTemplate restTemplate = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

		ResponseEntity<String> result = restTemplate.exchange(transactionSeriviceURL, HttpMethod.GET, entity,
				String.class);

		System.out.println(result);
		return null;
	}

	public static void deletetransaction(int id) {

		restTemplate.delete(transactionSeriviceURL + "/" + id);

	}

	public static void createtransaction(Transaction transaction) {
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<Transaction> request = new HttpEntity<Transaction>(transaction);
		ResponseEntity<Transaction> response = restTemplate.exchange(transactionSeriviceURL, HttpMethod.POST, request,
				Transaction.class);
		if (200 == response.getStatusCodeValue())
			System.out.println("cardtransaction Data Inserted Successfully");
	}

	public static void updatetransaction(Transaction transaction) {
		transactionSeriviceURL = "http://localhost:8081/api/v1/transaction/1";
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<Transaction> request = new HttpEntity<Transaction>(transaction);
		ResponseEntity<Transaction> response = restTemplate.exchange(transactionSeriviceURL, HttpMethod.PUT, request,
				Transaction.class);
		if (200 == response.getStatusCodeValue())
			System.out.println("cardtransaction Data Updated Successfully");
	}

	public static void main(String[] args) {
		// We should use these for above methods
		// Transaction transaction = new Transaction();
		 Transaction transaction = new Transaction( "1430", 1234, new Date() , new BigDecimal("4563.00"),"hai", 12, 13, "kumar6--", new Date(), "raj",new Date());
		 updatetransaction(transaction);
		// createtransaction(transaction);
		// Transaction transaction = gettransactionByIdx(1);
		// Transaction transaction =getAllTransaction();
		// deletetransaction(3);
	}
}
