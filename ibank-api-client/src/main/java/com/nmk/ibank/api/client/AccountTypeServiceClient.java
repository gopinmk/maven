package com.nmk.ibank.api.client;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.nmk.ibank.model.AccountType;

public class AccountTypeServiceClient {
	
	@Autowired
	static RestTemplate restTemplate = new RestTemplate();
	static String accounttypeSeriviceURL = "http://localhost:8081/api/v1/accountType";

	public static AccountType getAccountTypeByIdx(int id) {
		AccountType accountType = restTemplate.getForObject(accounttypeSeriviceURL + "/" + id, AccountType.class);
		System.out.println(accountType.getName() + " " + accountType.getDescription() + " " 
				+ accountType.getCreatedBy() + " " + accountType.getCreateDate() + " " + accountType.getModifiedBy() + " "
				+ accountType.getModifiedDate());
		return accountType;
	}

	public static AccountType getAllAccountType() {
		RestTemplate restTemplate = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		ResponseEntity<String> result = restTemplate.exchange(accounttypeSeriviceURL, HttpMethod.GET, entity, String.class);
		System.out.println(result);
		return null;
	}

	public static void deleteAccount(int id) {
		restTemplate.delete(accounttypeSeriviceURL + "/" + id);

	}

	public static void createAccountType(AccountType accountType) {
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<AccountType> request = new HttpEntity<AccountType>(accountType);
		ResponseEntity<AccountType> response = restTemplate.exchange(accounttypeSeriviceURL, HttpMethod.POST, request,
				AccountType.class);
		if (200 == response.getStatusCodeValue())
			System.out.println("Account Data Inserted Successfully");
	}

	public static void updateAccountType(AccountType accountType) {
		accounttypeSeriviceURL = "http://localhost:8081/api/v1/accountType/1";
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<AccountType> request = new HttpEntity<AccountType>(accountType);
		ResponseEntity<AccountType> response = restTemplate.exchange(accounttypeSeriviceURL, HttpMethod.PUT, request,
				AccountType.class);
		if (200 == response.getStatusCodeValue())
			System.out.println("AuditLog Data Updated Successfully");
	}

	public static void main(String[] args) {
		// We should use these for above methods
	//	 AccountType accountType = new AccountType("auiud","hai", new Date(), new Date(), 5768, 12, "kumar",
//					new Date(), "raj", new Date(), 21);
		// updateAccountType(accountType);
	//	 createAccountType(accountType);
		// AccountType accountType = getAccountTypeByIdx(1);
		AccountType accountType =getAllAccountType();
		// AccountType accountType = new AccountType();
		// deleteAuditlog(32);
	}

}
